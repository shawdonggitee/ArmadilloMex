# Armadillo +MKL 矩阵线性代数库及MATLAB混合编程

## 1 、cmake编译vs工程

在cmake编译过程中添加matlab的路径和Armadillo及Intel MKL的路径

## 2、matlab mex Armadillo 混合编程

1、matlab mex -setup 配置matlab mex编译为c++ 2017

2、将armadillo-9.900.3\mex_interface安装路径下的armaMex.hpp复制到armadillo-9.900.3\include路径下

3、仔细查看make.m的说明，使用matlab运行make.m    会使用matlab mex 编译armaMex_demo.cpp

4、编译成功之后运行run_demo.m 输出结果为matlab矩阵中的.+和.*运算结果

## 3 Armadillo 使用高性能Intel MKL库

1、下载安装Intel MKL库http://software.intel.com/en-us/intel-mkl/

### 3.1、matlab armadillo MKL混合编程

1、将armadillo-9.900.3\include\armadillo_bits安装路径下的config.hpp下有关于MKL的宏定义保留

2、make.m的头文件路径条件上MKL的头文件搜索路径，同时修改库路径

3、mex安装动态库。编译完生成mex文件之后 将matlab安装路径下E:\Program Files\MATLAB\R2019a\bin\win64中的icudt61.dll复制到mex文件中，将Intel MKL安装路径下的D:\IntelSWTools\compilers_and_libraries_2020.3.279\windows\redist\intel64_win\mkl的所有dll复制到mex中此时就能正常运行mex了。

### 3.2 VS下armadillo MKL编程

1、参考修改之后的cmake文件

2、vs属性选项中在安装完Intel MKL之后。在Intel Performance Library选项中的 Use Intek MKL选择Parallel

注：之所以想用matlab调用Armadillo进行混合编程，主要是想看是不是能进行加速。

验证：matlab的矩阵本来就用的是MKL库进行矩阵运算。Armadillo相当于在上层封装了一下，隐藏了底层库，底层库可以选择openblas等。默认的是blas和labpack比较慢，听说MKL库最快因此选择了它。复杂的矩阵运算确实和MATLAB的速度基本相同。