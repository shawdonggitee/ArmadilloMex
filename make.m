clear all;
% Get the architecture of this computer
is_64bit = strcmp(computer,'MACI64') || strcmp(computer,'GLNXA64') || strcmp(computer,'PCWIN64');
%---------------------------------------------------------------------------------------------- 
%% The configuration of compiler
% You need to modify this configuration according to your own path of armadillo 
out_dir='./'; 
%% armadillo
CPPFLAGS = ' -O -DNDEBUG -I.\ -IG:\learnCppCode\armadillo-9.900.3\armadillo-9.900.3\include -ID:\IntelSWTools\compilers_and_libraries_2020.3.279\windows\mkl\include';%include path
%LDFLAGS = ' -LG:\learnCppCode\armadillo-9.900.3\armadillo-9.900.3\examples\lib_win64'; %lib path
%LIBS = ' -lblas_win64_MT -llapack_win64_MT';%lib
LDFLAGS = ' -LD:\IntelSWTools\compilers_and_libraries_2020.3.279\windows\mkl\lib\intel64_win -LD:\IntelSWTools\compilers_and_libraries_2020.3.279\windows\redist\intel64_win\mkl';
%LIBS =' -lmkl_core -lmkl_core_dll -lmkl_intel_ilp64 -lmkl_intel_ilp64_dll -lmkl_intel_lp64 -lmkl_intel_lp64_dll -lmkl_intel_thread -lmkl_intel_thread_dll -lmkl_rt -lmkl_sequential -lmkl_sequential_dll -lmkl_tbb_thread -lmkl_tbb_thread_dll';
LIBS=' -lmkl_rt -lmkl_core -lmkl_core_dll -lmkl_intel_thread -lmkl_intel_thread_dll';
%% 
if is_64bit 
    CPPFLAGS = [CPPFLAGS ' -largeArrayDims']; 
end 
 
%% add your files here!!  %source file
compile_files = {
    %the list of your code files which need to be compiled
    %'armaMex_demo.cpp'
    'test_time_pinv.cpp'
    };
%----------------------------------------------------------------------------------------------
 
%----------------------------------------------------------------------------------------------
%% compiling
for k = 1 : length(compile_files) 
    str = compile_files{k}; 
    fprintf('compilation of: %s\n', str); 
    str = [str ' -outdir ' out_dir CPPFLAGS LDFLAGS LIBS ] 
    args = regexp(str, '\s+', 'split') 
    args{:}
    mex(args{:}); 
end
fprintf('Congratulations, compilation successful!!!\n');
