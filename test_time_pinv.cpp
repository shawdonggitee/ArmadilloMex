#include <iostream>
#include <armadillo>
#include "armaMex.hpp"
using namespace std;
using namespace arma;

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    mat A = armaGetPr(prhs[0]);
    mat B = pinv(A);
    plhs[0] = armaCreateMxMatrix(B.n_rows,B.n_cols);
    armaSetData(plhs[0],B);
    
}